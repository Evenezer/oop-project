# PPI Network Analysis Tool
This is a Python command-line tool for generating and analyzing protein-protein interaction (PPI) networks. The tool can generate node/edge lists from PPI data, create graph images, find shortest paths between nodes, and generate summary statistics.

## Requirements
Python 3.x
click 8.0 or newer
networkx 2.6 or newer
matplotlib 3.4 or newer
Installation
Clone or download this repository to your local machine.
Install the dependencies using pip:
Copy code
pip install -r requirements.txt
To use the tool, run the following command in your terminal from the root of the repository:
css
Copy code
python ppi_network_tool.py [COMMAND] [OPTIONS]
Commands
The following commands are available:

## compile
Generates node/edge lists from a PPI file.

## python

python ppi_network_tool.py compile [OPTIONS] PPI NODES EDGES
## create
Creates a graph image from a PPI file or node/edge lists.



python ppi_network_tool.py create [OPTIONS] OUTPUT_PATH
path
Generates a graph image highlighting specific paths between the given source and target.


python ppi_network_tool.py path [OPTIONS] OUTPUT_PATH
stats
Generates summary statistics about a PPI network.


python ppi_network_tool.py stats [OPTIONS]
For more information about each command and their options, run:


python ppi_network_tool.py [COMMAND] --help